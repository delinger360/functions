const isCharInt = (char) => Number.isInteger(parseInt(char));

const validate = (...strings) => strings.every(str => {
  if (typeof str !== 'string') return false;
  return str.split('').every(isCharInt) || str === '';
});

const stringToNumbers = (string) => {
  if (string === '') return [0];
  const chars = string.split('');
  return chars.map(char => Number(char));
};

const getSum = (str1, str2) => {
  const valid = validate(str1, str2);
  if (!valid) return false;
  const first = stringToNumbers(str1);
  const second = stringToNumbers(str2);
  const size = Math.max(first.length, second.length);
  const sum = Array.from({length: size});
  return sum.map((_, idx) => [first, second]
    .map(array => array[idx] || 0)
    .reduce((total, number) => total + number, 0))
    .join('');
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  const posts = listOfPosts.filter(post => post.author === authorName);
  const comments = listOfPosts.map(post => post.comments || [])
    .reduce((result, comment) => result.concat(comment))
    .filter(comment => comment.author === authorName);
  const postsCount = 'Post:' + posts.length;
  const commentsCount = 'comments:' + comments.length;
  return postsCount + ',' + commentsCount;
};

const giveChange = (balance, payment) => {
  if (!balance.canPay) return balance;
  if (payment === 25) return {...balance, onHand: balance.onHand + payment};
  const change = payment - 25;
  const canChange = (balance.onHand - change) >= 0;
  return canChange ? {...balance, onHand: balance.onHand + change} : {...balance, canPay: false};
};

const tickets=(people)=> {
  const first = people.shift();
  if (first > 25) return 'NO';
  const processed = people.reduce((balance, payment) => 
    giveChange(balance, payment), { onHand: first, canPay: true });
  return processed.canPay ? 'YES' : 'NO';
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
